const NodeProperties = [
  'each', 'elements', 'enum',
  'pairs', 'multiple',
  'optional', 'pattern', 'properties',
  'type', 'validate', 'value'
];


class Schema {
  constructor(description) {
    this.tree = parseDescription(description);
  }

  validate(target, options = {}) {
    let errors = validate(this.tree, target, options);

    return errors.map((err) => {
      let path = `${formatPath(err.path)}`;

      switch (err.type) {
        case 'illegal': return `${path} is illegal`;
        case 'pattern': return `${path} must match the pattern ${err.expected}`;
        case 'required': return `${path} must be present`;
        case 'type': return `${path} must be of type ${err.expected}`;
        case 'value': return `${path} must be equal to '${err.expected}'`;
        default: return err.format(path);
      }
    });
  }
}

const Validators = {
  integer: {
    type: Number,
    validate: (value, _opts) => {
      if (!Number.isInteger(value)) {
        return [{ format: (path) => `${path} must be an integer` }];
      }
    }
  },
  bounded(min, max, tol = 0) {
    return {
      type: Number,
      multiple: [
        this.number,
        { validate: (value, _opts) => {
          if (value - min < tol) {
            return [{ format: (path) => `${path} must be greater than ${min}` }];
          } if (value - max > tol) {
            return [{ format: (path) => `${path} must be lower than ${max}` }];
          }
        } }
      ]
    };
  },
  number: {
    type: Number,
    validate: (value, _opts) => {
      if (isNaN(value)) {
        return [{ format: (path) => `${path} must be a number` }];
      } if (!isFinite(value)) {
        return [{ format: (path) => `${path} must be finite` }];
      }
    }
  }
};

exports.Validators = Validators;
exports.Schema = Schema;


function formatPath(path) {
  return path.length < 1
    ? 'the root object'
    : `'` + path
      .map((segment, index) => typeof segment === 'number'
        ? `[${segment}]`
        : (index === 0 ? '' : '.') + segment)
      .join('') + `'`;
}


function parseDescription(description) {
  if (Array.isArray(description)) {
    return {
      type: Array,
      ...(description.length > 1
        ? { elements: description.map((child) => parseDescription(child)) }
        : { each: parseDescription(description[0]) })
    };
  }

  if (description.constructor === Object) {
    if (Object.keys(description).every((key) => NodeProperties.includes(key))) {
      return {
        ...description,
        ...(description.each && { each: parseDescription(description.each) }),
        ...(description.elements && { elements: description.elements.map((child) => parseDescription(child)) }),
        ...(description.enum && { enum: description.enum.map((child) => parseDescription(child)) }),
        ...(description.pairs && { pairs: parseDescription(description.pairs) }),
        ...(description.properties && { properties: Object.fromEntries(Object.entries(description.properties).map(([key, child]) => [key, parseDescription(child)])) })
      };
    }

    return {
      type: Object,
      properties: Object.fromEntries(Object.entries(description).map(([key, value]) => [key, parseDescription(value)]))
    };
  }

  if (description.constructor === RegExp) {
    return {
      type: String,
      pattern: description
    };
  }

  if (typeof description === 'function') {
    return { type: description };
  }

  if (typeof description !== 'object') {
    return { type: description.constructor, value: description };
  }

  if (description instanceof Schema) {
    return description.tree;
  }

  throw new Error('Illegal description');
}


function validate(node, target, options) {
  if (target === void 0 || target === null) {
    if (node.optional) {
      return [];
    } else {
      return [{ type: 'required', path: [] }];
    }
  }

  if (node.multiple) {
    return node.multiple.flatMap((childNode) => validate(childNode, target, options));
  }

  if (node.type && target.constructor !== node.type && !(
      options.allowUndefinedType
    && node.type === Object
    && target.constructor === void 0
  )) {
    return [{ type: 'type', path: [], expected: node.type.name }];
  }

  if (('value' in node) && target !== node.value) {
    return [{ type: 'value', path: [], expected: node.value }];
  }

  if (node.properties) {
    let errors = [];

    for (let [key, childNode] of Object.entries(node.properties)) {
      let valueErrors = validate(childNode, target[key], options);

      for (let err of valueErrors) {
        err.path.unshift(key);
      }

      errors.push(...valueErrors);
    }

    if (options.strict) {
      for (let key of Object.keys(target)) {
        if (!(key in node.properties)) {
          errors.push({ type: 'illegal', path: [key] });
        }
      }
    }

    return errors;
  }

  if (node.each) {
    let errors = [];

    for (let index = 0; index < target.length; index++) {
      let valueErrors = validate(node.each, target[index], options);

      for (let err of valueErrors) {
        err.path.unshift(index);
      }

      errors.push(...valueErrors);
    }

    return errors;
  }

  if (node.elements) {
    let errors = [];

    for (let index = 0; index < node.elements.length; index++) {
      let valueErrors = validate(node.elements[index], target[index], options);

      for (let err of valueErrors) {
        err.path.unshift(index);
      }

      errors.push(...valueErrors);
    }

    if (options.strict) {
      for (let index = node.elements.length; index < target.length; index++) {
        errors.push({ type: 'illegal', path: [index] });
      }
    }

    return errors;
  }

  if (node.enum) {
    let minErrors = null;

    for (let child of node.enum) {
      let valueErrors = validate(child, target, options);

      if (!minErrors || valueErrors.length < minErrors.length) {
        minErrors = valueErrors;
      }

      if (minErrors.length < 1) {
        return [];
      }
    }

    return minErrors;
  }

  if (node.pattern && !node.pattern.test(target)) {
    return [{ type: 'pattern', path: [], expected: node.pattern.source }];
  }

  if (node.pairs) {
    return Object.entries(target).flatMap(([key, value]) =>
      validate(node.pairs, value, options).map((err) => ({ ...err, path: [...err.path, key] }))
    );
  }

  if (node.validate) {
    let result = node.validate(target, options);

    if (Array.isArray(result)) {
      return result.map((err) => ({ path: [], ...err }));
    } else if (!result && result !== void 0) {
      return [{ type: 'illegal', path: [] }];
    }
  }

  return [];
}
